---
title: 时态
type: term
---

时态 / shítài / tense 

# present 
--------------------------------------------------------------------------------

* __在 / zài__ -> present participle marker
	+ when placed before a verb indicates the action is ongoing
	+ structure: [S] [在] [V]   
	+ 她在说 -> she is speaking
	+ 她在看书 -> she is reading

* __着 / zhe__ -> auxiliary to indicate continued action or state
	+ added to verb or adjective to indicate continued action or state
	+ often the particle 呢 is also added to the end of the sentence
	+ structure: [V] [着] ([呢])
	+ 雪正下着呢 -> xuě zhèng xià zhe ne -> it's showing

# past 
--------------------------------------------------------------------------------

* generally use 了, the perfective aspect marker, to indicate a completed action

* can use 没有 + [verb] to indicate an action didn't happen in the past
	+ 我没有说汉语 -> I didn't speak chinese
	+ 我没有在说汉语 -> I wasn't speaking chinese

# future 
--------------------------------------------------------------------------------

* __要 / yào__ -> will, be going to
  + structure: [S] [要] [V] [O]
  + 爸爸要去家 -> father [will] go home
  + note 要 also means to want/wish/desire. unsure how to differentiate between 
    will and want

* __会 / huì__ -> will 
	+ structure: [S] [会] [V] [O]
	+ 明天你会来马？ -> will you come tomorrow?
	+ 明天会下雨马？ -> tomorrow [will] rain? -> will it rain tomorrow?

# perfective aspect, 了
--------------------------------------------------------------------------------

* __了 / le__ -> the perfective aspect marker

* base case:
	+ used to indicate that an action is completed
	+ structure: [S] [V] [le]
	+ [in general 了 comes after the verb. sometimes it comes after the object]
	+ 我们买了 -> we bought it
	+ 我们都去了 -> we all went

* when time is specified:
	+ structure: [S] [time] [V] [le] [O] or [time] [S] [V] [le] [O]
	+ 我中午见了姑娘 -> I met a girl at noon

* when quantity is specified:
	+ structure: [S] [V] [le] [number] [measure word] [O]
	+ 老师问了六个问题 -> the teacher asked six questions

* sometimes 了 comes after the object
	+ structure: [S] [V] [O] [le]
	+ 上个月我去台湾了 -> last month I went to Taiwan

* with consecutive actions: only need one "le"
	+ [S] [V] [place] [V phrase] [le]
	+ 昨天她来我家吃饭了 -> yesterday she came to my house and ate a meal

# experiential aspect
--------------------------------------------------------------------------------

* 过: indicates something has happened before (experiential)
	+ 我学过汉语 -> I have studied chinese before

# imperatives
--------------------------------------------------------------------------------

* __不要 / bú yào__ -> "do not" [literally "don't want"]
	+ 不要说英语 -> do not speak english
