---
title: 2020 九月
time: 202009
type: term
domain: 中文
---

# 2020年9月1日

- grammar: degree with 多
- grammar: not very with 不太
	+ can use 不太 with psychological verbs, e.g. 喜欢, 想, 明白 etc
- new vocab:
	+ 黄河 → huánghé → yellow river
	+ 久 → jiǔ → long (time)
	+ 待 → dāi → to treat. colloquially “to stay”
	+ 重 → zhòng → heavy
	+ 这些 → zhèxiē → these few things
	+ 冬天 → dōngtiān → winter
	+ 地方 → dìfang -> place, location, area. can mean abstract place, doesn’t have to be a concrete place
	+ 聪明 → cōngmíng → smart, brilliant, intelligent
	+ 怕 → pà → to be afraid of
	+ 一件事 → yī jiàn shì → the cheddar
	+ 别人→ biérèn → other peoplek
	+ 的时侯 → de shí hòu → when 


# 2020年9月2日

- grammar point: and with 和
- grammar point: after a specific time with 以后
- new vocab:
	+ 爷爷 → yéye
	+ 奶奶 → nǎinai
	+ 可以 → kěyǐ → apparently it means “ok” [adj]. unclear
	+ 午饭 → wǔfàn → lunch
	+ 吃完 → chīwán → finish eating. in general wán can go after words to mean “finish doing the thing"
	+ 更 → gèng → more, even more, e.g. 更大 → even bigger
	+ 会 → can be used as an adjective phrase, “will be even bigger"
	+ 半个 → bàn gè → half hour
	+ 班 → bān → means “work”. can be prepended with 上 and 下 to mean “go to work” or “get off work"
	+ 跟 → gēn → together
	+ 干爸爸 → gēn bā bā


# 2020年9月3日

- grammar point: before a specific time with 以前
- grammar point: time words and order
- new vocab
	+ 公园 → gōngyuán → park
	+ 分 → fēn → divide, separate, distinguish
	+ 分手 → fēnshǒu → to break hands [up]
	+ 当 → dāng → to work as, serve as, manage, in charge of, preposition: when
	+ 总统 → zǒngtǒng → president
	+ 头发 → tóufa -> hair
	+ bànggōngshì → office [can’t find the characters]
- read half of page two about 张的鼻子


# 2020年9月5日

- grammar: age with 岁
	+ note: no verb comes after the number. is apparently a “measure word"
- grammar: measure word 个
	+ use gè by itself when you want to emphasise there’s only one thing
- new vocab:
	+ 老外 → lǎowài → foreigner
	+ 鬼佬 → guǐlǎo → “gringo"

# 2020年9月6日

- grammar: structure of dates
- grammar: structure of days of the week
- no vocab or 读中文 [was rather late]

# 2020年9月7日

- grammar point: structure of numbers
- grammar point: structure of times

# 2020年9月8日
- grammar point: possession without de
- grammar point: completed actions with le [1]
	+ clicked on the “advanced use of le” link
	+ can use double le for emphasis, for big quantities, etc
	+ can’t use with a psychological verb
	+ completion in the future: [I] [verb] le [verb] [object] → after I do the verb, do another verb
- new vocab:
	+ 有名: yǒumíng → famous
	+ 台湾: Táiwān 
	+ 周 → zhōu → week [other]

# 2020年9月9日

- new vocab: 
	+ 纸 → zhǐ → paper
	+ 上课 → to go/give a class
	+ 快点儿 → kuài diǎnr → hurry up then
- grammar point: not anymore with 了
	+ structure: 不 + [verb phrase] + 了 or 没有 “”
	+ 已经 for emphasis. goes before the 不
- grammar point: “now” with 了


# 2020年9月12日

- grammar point: possession with 的
- grammar point: questions with 呢
- new vocab
	+ 周末 → zhōumò → weekend
	+ 随便 → sui bian → anything

# 2020年9月13日

- grammar point: sentence-final interjection 阿
- grammar point: softening speech with 吧
- new vocab:
	+ 香 -> xiāng -> adjective for aromatic/fragrant
	+ 是阿 -> urgent/vehement affirmation 
	+ 行阿 -> xīng a -> all right, ok. also the japanese word for "to go"
	+ 小心 -> xiǎoxīn -> careful. because you're a craven weakling
	+ 心 -> xīn -> the heart
	+ 傻 -> shǎ -> stupid
	+ 等 -> děng -> wait
	+ 迟 -> chí -> late
	+ 地方 -> dìfang -> place

# 2020年9月14日

- grammar point: suggestions with 吧
- grammar point: directional verbs 来 and 去
- new vocab:
	+ 香港 -> xiānggǎng -> hong kong
	+ [name] 路 -> [name] road, i.e. nanjing road
- watched a video on pronouncing the neutral tone
	+ there's four following pitches, one for each tone
- watched a video on 吧 from chinese 101

# 2020年9月15日

- grammar point: existence in a place with 在
- grammar point: possession with 有
- new vocab:
	+ 楼上 -> lóushàng -> upstairs
	+ 楼下 -> lóuxià -> downstairs?
	+ 帅 -> shuài -> handsome, dashing
	+ 自己 -> zìjǐ -> myself
	+ 不用 -> búyòng -> need not
	+ 没问题 -> méiwèntí -> no problem!
	+ 我自己来 -> I'll do it myself
- started popup chinese podcast, absolute beginner "let me do it myself"

# 2020年9月16日

- grammar point: possession with 有
- grammar point: polite requests with 请
- new vocab:
	+ 洗手间 -> xǐshǒujiān -> bathroom
	+ 离开 -> líkāi -> to leave, depart, desert 
	+ 迟到 -> chídào -> late
	+ 一共 -> yīgòng -> altogether
- finished podcast. feel good. 

# 2020年9月18日

- grammar point: using the verb 叫
- grammar point: using the verb 去
- new vocab:
	+ 超市 -> chāoshì -> supermarket
	+ 有时 -> yǒushí -> sometime
	+ 左右 -> zuǒyòu -> around
	+ 存 -> cún -> store, keep, deposit
	+ 陪陪 -> péipéi -> to spend time with, accompany
	+ 没什么 -> never mind

# 2020年9月19日

- grammar: ability with 能
- grammar: ability with 会
- new vocab:
	+ 网 -> wǎng -> net or internet
	+ 宝宝 -> bǎobao -> baby
	+ 早点 -> zǎo diǎn -> a little earlier
	+ 小声 -> xiào shēng -> to lower voice
	+ 号码 -> hàomǎ -> phone number

# 2020年9月20日

- grammar: expressing "will" with 会
- grammar: expressing "would like to" with 想
- vocab:
	+ 跟 -> gēn-> to follow [verb] or with [preposition]
	+ 花 -> huá -> to spend
	+ 父母 -> fùmǔ -> parents
	+ 小说 -> novel
	+ 前包 -> qiánbāo

# 2020年9月21日

- grammar: "how to" with 怎么
- grammar: indicating location with 在 before verbs
- vocab:
	+ 外滩 -> Wàitān -> the Bund
	+ 女孩子 -> nǔháizi -> girl
	+ 一直 -> yīzhí -> always
	+ 床 -> chuáng -> bed
	+ 厕所 -> cèsuǒ -> bathroom/lavatory
- 家 is a measure word for families or businesses/companies
	+ 这家公司 -> this company
- set up deck for measure words

# 2020年9月22日

- grammar: negation of past actions with 没有
- grammar: basic sentence structure
- vocab:
	+ 啤酒 -> píjiǔ -> beer

# 2020年9月26日

- grammar: connecting nouns with 是
- grammar: expressing "excessively" with 太
- vocab:
	+ 脏 -> za1ng -> dirty
	+ 

# 2020年9月28日

- grammar: noun + 很 + adj
- grammar: expressing "some" with 一些
- vocab:
	+ 同事 -> tóngshì -> coworkers
	+ 快 -> kuài -> fast, soon, quickly
	+ 借-> jiè -> lend
	+ 放 -> fàng -> put, "put sugar in coffee"
	+ 糖 -> táng -> sugar
- bad: missed chinese 4 times last week
- listening: battery, assault, future
	+ sentence final particle 的: to express a mood of "definiteness"
	+ e.g. 我会大你的 -> I will hit you

# 2020年10月1日

- grammar: counting money
- grammar: measure words in quantity questions
- grammar: asking how something is with 怎么样
- vocab
	+ 件 -> jiàn -> measure word for furniture, clothing, matters 
	+ 班 -> bān -> means class, or shift in a workplace
	+ 老 -> lǎo -> dark, for dark green, dark red
