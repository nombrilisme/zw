---
title: 2020 十月
time: 202010
domain: 中文
type: term
---

# 2020年10月1日

- grammar: counting money
- grammar: measure words in quantity questions
- grammar: asking how something is with 怎么样
- vocab
	+ 件 -> jiàn -> measure word for furniture, clothing, matters 
	+ 班 -> bān -> means class, or shift in a workplace
	+ 老 -> lǎo -> dark, for dark green, dark red

# 2020年10月2日

- grammar: placement of question words
- grammar: questions with "le ma"
- grammar: yes/no questions with 马
- vocab:
	+ 区 -> qū -> district, borough, area
	+ 苦 -> kǔ -> bitter
	+ 外国人 -> wàiguórén -> foreigner
	+ 上网 -> shàngwǎng -> to go online
	+ 邮件 -> yóujiàn -> mail, also means email colloquially
	+ 收到 -> shòudào -> to receive [can drop the dao]
	+ 恩 -> ǹg -> agreement, positive affirmation

# 2020年10月5日

- grammar: the shi... de construction
- grammar: comparing 不 and 没
- vocab:
	+ 出生 -> chūshēng -> born
	+ 骑 -> qí -> ride
	+ 自行车 -> zìxíngchē -> bicycle
	+ 平时 -> píngshí -> usually
	+ 对不对 -> is that right?
	+ 付 -> fù -> pay

# 2020年10月6日

- grammar: continuation with 还
- grammar: emphasis with 就是
- vocab:
	+ 考虑 -> kǎolù -> to think, contemplate
	+ 旧 -> jiù -> old, used, secondhand
	+ 游戏 -> yóuxì -> video game
	+ 可是 -> kěshì -> but [colloquial form of 但是], also can place in front of words for emphasis
	+ 新鲜 -> xīnxiān -> fresh
	+ 出门 -> chūmén -> leave the house
	+ 解释 -> jiěshì -> explain
	+ 遍 -> biàn -> measure word for "one time", "once through". also means "everywhere"
	+ 对 -> duì -> to treat, cope with, deal with
	+ 笨蛋 -> bèndàn -> fool, idiot
	+ 骗子 -> piànzi -> conman
	+ 地地道道 -> dìdìdàodào -> out and out, outright, one hundred percent, etc
	+ 参加 -> cānjiā -> to enter, participate 
	+ 比赛 -> bǐsài -> competition, to compete
	+ 输 -> shū -> to lose
	+ 这样 -> zhèyàng -> this way, so, such, equivalent to 这么?
	+ 担心 -> dānxīn -> worried
	+ 优势 -> yōushì -> strengths, superiority
	+ 一个人 -> yīgèrén -> alone

# 2020年10月8日

- grammar: emphasising quantity with 都
- grammar:
- vocab: 
	+ 上学 -> shàngxué -> to go to school
	+ 年轻 -> niánqīng [ren] -> young [people]
	+ 轻 -> qīng -> light, i.e. lightweight
	+ 城市 -> chéngshì -> city
	+ 忘 -> wàng -> to forget
	+ 吃辣 -> chīlà -> spicy food
	+ 四川人 -> sìchuānrén -> sichuanese people

# 2020年10月13日

- grammar: "about to" with 就要
- grammar: expressing "again" in the future with 在
- vocab:
	+ 过年 -> guònián -> verb, to observe chinese new year's day, colloquially also means next year
	+ 放假 -> fàngjià -> to be on holiday
	+ 试 -> shì -> try, test
	+ 复习 -> fùxí -> review, as in study 
	+ 点 -> diǎn -> select, choose, order (as in order a meal)
	+ 瓶 -> píng -> bottle, also a measure word for "a bottle of"
	+ 急 -> jí -> be anxious, to worry
	+ 聊 -> liáo -> to chat
	+ 一会儿 -> yíhuǐr -> a little bit (longer)

# 2020年10月17日

- grammar: already with 都
- grammar: already with 已经
- vocab:
	+ 世纪 -> shìjì -> century
	+ 扔掉 -> rēngdiào -> to throw away
	+ 烦 -> fán -> to annoy, superfluous
	+ 道歉 -> dàoqiàn -> to apologise
	+ 感冒 -> gǎnmào -> the common cold, or to catch a cold

# 2020年10月19日

- grammar: switched to A1!
- grammar: using 姓 for surname (from A1)
