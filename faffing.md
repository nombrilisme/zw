---
title: 我是美国人。
type: term
draft: true
---

我是美国人。

我是一个要学汉语的美国人。

# LA PROBLEME DE MONTY HALL? JE NE SAIS RIEN

我有三个杯子。一个杯子下有手机。哪一个他？

- s1: I have three cups -> I have three cups
  - nothing weird here. standard SVO. 

- s2: under one of them is a phone -> one cup-under [there is] phone
  - uses XVS locative inversion [also serves to topicalise the sentence. we 
    already know about the cups]
  - also true in the english version [as opposed to saying "there is a phone 
    under one cup"]

- s3: which one is it? -> which one it?
  - I think this works? don't need 是 because the predicate is [or refers to, at 
    least] a locative phrase?

# LES CONNERIES

亚论刚刚告诉我你正在找工作。告诉我如果你想试一试高盛。
- yàlún gānggāng gàosù wǒ nǐ zhèngzài zhǎo gōngzuò. gàosù wǒ rúguǒ nǐ xiǎng 
  shìyīshì gāoshèng.
- AC just tell I/me you [in the process of] lookfor/seek work. tell me if you 
  consider [try out] [goldman sachs]. 
- how do I say "fuck that, fuck goldman sacks, and fuck le capitalisme? regardes 
  la chinoise. godard pour toujours!"
