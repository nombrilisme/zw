---
title: 2021 三月
type: term
domain: 中文
time: 202103
---

# 2021年3月16日

- woohoo! 中文！
- 我是大坏蛋！ 
- 坐火车 -> "on the train"
- did grammar review for an hour
- good grammar day
- did it properly
- spent one hour today

# 2021年3月17日

reading day: 

vocab
- 带 / dài / to take, bring
- 阿姨 / ā yí / auntie
- 小朋友 / child, little friend, "friendly" way of saying child
- 摸 / mō / touch, feel, pat, grope

grammar?
- 看了看: verb le verb. "looked at?" 

# 2021年3月18日

- listening day: the second vaccination

# 2021年3月19日

- grammar day. review.  

scoreboard:

A = + 2 + 2 + 14 + 1 + 9 + 90  
V = + 5 - 1 + 15 + 2 + 10 + 100 - 2   

# 2021年3月20日

- reading day: 有小三 "having a mistress"
- 爸爸回来了 -> baba returned (home)
- 门外 -> outside the door
	+ "head final" grammar? the modifiers always come after
	  the thing. perhaps with a few exceptions...
- adding 了 in multi verb sentences: you'll always be fine if you put the 
  了 after the last verb

vocab
- 让 -> to let someone do something, or *make* someone do something, 
  (context dependent)
- 金门 -> enter the door, "come in"

listening (while making risotto)
- 我是不是tom cruise

# 2021年3月21日

listening
- adding "zhen" in front of things -> really
- 真对不起 -> I'm really sorry
- 真好 -> really good

# 2021年3月22日

- reading day: continue "having a mistress"
- exercise: we dialed a chinese number to see if we'd get the 
  message (we didn't)

# 2021年3月23日

- watched mandarin corner youtube channel. the hong kong video with 
  pinyin only. watched the whole 54 minute video. 
- 给我就好 -> gěi wǒ jiù hǎo -> "give me it good?"
- 可以了 -> kěyǐ le -> I got it, "did it", etc. 

# 2021年3月25日

- grammar day: review
- 毛 -> máo -> tone 2

# 2021年3月27日

- grammar day
- important thing about dates: you say the numbers one by one, you 
  don't say the "number"
  	+ 2021 -> / 而零而一
- new cards about big numbers
- almost done with grammar deck, only like 10-30 cards left

# 2021年3月30日

- watching mandarin corner
- 香港 -> xiānggǎng -> hong kong
- 夜市 -> yèshì -> nightmarket
- 产品 -> chǎnpǐn -> goods
- 特地 -> tèdì -> especially
- 当地 -> dāngdì -> local, + thing means "of the local thing"
- lots of 呢s everywhere, bothers
- 的 strikes again, freakin' everywhere
- general inability to link clauses

# 2021年3月31日

- grammar day
